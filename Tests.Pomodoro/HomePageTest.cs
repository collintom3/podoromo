using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using FluentAssertions;
using Microsoft.JSInterop;
using System.Threading.Tasks;
using System.IO;
using System.Text.Json;
using System.Timers;
using pomodoro.Components.Pages;

public class HomeTests : TestContext
{
    //[Fact]
    //public void StartTimer_ShouldInitializeAndStartTimer()
    //{
    //    // Arrange
    //    var homeComponent = RenderComponent<Home>();
    //    var timerField = homeComponent.Instance.GetType().GetField("timer", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

    //    // Act
    //    homeComponent.Find("button:contains('D�marrer')").Click();
    //    var timer = timerField?.GetValue(homeComponent.Instance) as System.Timers.Timer;

    //    // Assert
    //    timer.Should().NotBeNull();
    //    timer?.Enabled.Should().BeTrue();
    //}

    [Fact]
    public async Task PauseTimer_ShouldStopAndNullifyTimer()
    {
        // Arrange
        var homeComponent = RenderComponent<Home>();
        await homeComponent.InvokeAsync(() => homeComponent.Instance.StartTimer());
        var timerField = homeComponent.Instance.GetType().GetField("timer", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

        // Act
        homeComponent.Find("button:contains('Pause')").Click();
        var timer = timerField?.GetValue(homeComponent.Instance) as System.Timers.Timer;

        // Assert
        timer.Should().BeNull();
    }

    [Fact]
    public async Task UpdateTimer_ShouldUpdateCurrentTimeAndSwitchBetweenWorkAndBreak()
    {
        // Arrange
        var homeComponent = RenderComponent<Home>();
        await homeComponent.InvokeAsync(() => homeComponent.Instance.StartTimer());

        // Act
        var timerField = homeComponent.Instance.GetType().GetField("timer", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        var timer = timerField?.GetValue(homeComponent.Instance) as System.Timers.Timer;

        if (timer != null)
        {
            // Simulate the Elapsed event
            var updateTimerMethod = homeComponent.Instance.GetType().GetMethod("UpdateTimer", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            updateTimerMethod?.Invoke(homeComponent.Instance, new object[] { timer });

            // Assert
            var currentTimeField = homeComponent.Instance.GetType().GetField("currentTime", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var currentTime = (double?)currentTimeField?.GetValue(homeComponent.Instance);

            currentTime.Should().NotBeNull();
            currentTime.Should().BeLessThan(45 * 60); // Assuming workDuration is initially set to 45 minutes
        }
    }

    [Fact]
    public async Task SaveSessionDataToJsonServer_ShouldWriteJsonFile()
    {
        // Arrange
        var homeComponent = RenderComponent<Home>();
        var sessionCountField = homeComponent.Instance.GetType().GetField("sessionCount", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        var minutesWorkedField = homeComponent.Instance.GetType().GetField("minutesWorked", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

        sessionCountField?.SetValue(homeComponent.Instance, 3);
        minutesWorkedField?.SetValue(homeComponent.Instance, 135); // 2 hours 15 minutes

        // Act
        await homeComponent.InvokeAsync(() => homeComponent.Instance.SaveSessionDataToJsonServer());

        // Assert
        var fileContent = await File.ReadAllTextAsync("sessionData.json");
        fileContent.Should().Contain("\"SessionCount\":3");
        fileContent.Should().Contain("\"MinutesWorked\":135");
    }

    //[Fact]
    //public async Task LoadSessionDataFromJsonClient_ShouldLoadSessionData()
    //{
    //    // Arrange
    //    var jsRuntimeMock = new Mock<IJSRuntime>();
    //    jsRuntimeMock.Setup(js => js.InvokeAsync<string>("localStorage.getItem", It.IsAny<object[]>()))
    //                 .ReturnsAsync("{\"SessionCount\": 3, \"MinutesWorked\": 135}");

    //    Services.AddSingleton(jsRuntimeMock.Object);

    //    var homeComponent = RenderComponent<Home>();

    //    // Act
    //    await homeComponent.InvokeAsync(() => homeComponent.Instance.LoadSessionDataFromJsonClient());

    //    // Assert
    //    var sessionCountField = homeComponent.Instance.GetType().GetField("sessionCount", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
    //    var minutesWorkedField = homeComponent.Instance.GetType().GetField("minutesWorked", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

    //    var sessionCount = (int?)sessionCountField?.GetValue(homeComponent.Instance);
    //    var minutesWorked = (double?)minutesWorkedField?.GetValue(homeComponent.Instance);

    //    sessionCount.Should().Be(3);
    //    minutesWorked.Should().Be(135);
    //}

    [Fact]
    public async Task DownloadSessionDataAsJson_ShouldInvokeJSRuntime()
    {
        // Arrange
        var jsRuntimeMock = new Mock<IJSRuntime>();
        jsRuntimeMock.Setup(js => js.InvokeAsync<object>("downloadFile", It.IsAny<object[]>())).Verifiable();

        Services.AddSingleton(jsRuntimeMock.Object);

        var homeComponent = RenderComponent<Home>();

        // Act
        await homeComponent.InvokeAsync(() => homeComponent.Instance.DownloadSessionDataAsJson());

        // Assert
        jsRuntimeMock.Verify(js => js.InvokeAsync<object>("downloadFile", It.Is<object[]>(args => VerifyDownloadFileArguments(args))), Times.Once);
    }


    private bool VerifyDownloadFileArguments(object[] args)
    {
        if (args.Length != 2)
            return false;

        if (args[0]?.ToString() != "sessionData.json")
            return false;

        var jsonString = args[1]?.ToString();
        if (string.IsNullOrEmpty(jsonString))
            return false;

        return jsonString.Contains("\"SessionCount\":") && jsonString.Contains("\"MinutesWorked\":");
    }


}
