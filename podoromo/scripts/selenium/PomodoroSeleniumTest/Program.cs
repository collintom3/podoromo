﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

class Program
{
    static void Main(string[] args)
    {
        // Set up the ChromeDriver
        IWebDriver driver = new ChromeDriver();
        driver.Navigate().GoToUrl("http://localhost:5248/"); // Change the URL to your application's URL

        // Wait until the page is fully loaded
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        wait.Until(drv => drv.FindElement(By.CssSelector("h1")).Displayed);

        // Find the start button and click it
        IWebElement startButton = driver.FindElement(By.XPath("//button[text()='Démarrer']"));
        startButton.Click();

        // Wait for a few seconds to simulate some passage of time
        System.Threading.Thread.Sleep(5000);

        // Find the pause button and click it
        IWebElement pauseButton = driver.FindElement(By.XPath("//button[text()='Pause']"));
        pauseButton.Click();

        // Wait for a moment
        System.Threading.Thread.Sleep(2000);

        // Find the download button and click it
        IWebElement downloadButton = driver.FindElement(By.XPath("//button[text()='Télécharger les Données']"));
        downloadButton.Click();

        // Verify the session data
        IWebElement sessionCount = driver.FindElement(By.XPath("//p[contains(text(), 'Sessions complètes')]"));
        Console.WriteLine("Session Count: " + sessionCount.Text);

        IWebElement minutesWorked = driver.FindElement(By.XPath("//p[contains(text(), 'Minutes travaillées')]"));
        Console.WriteLine("Minutes Worked: " + minutesWorked.Text);

        // Close the browser
        driver.Quit();
    }
}
